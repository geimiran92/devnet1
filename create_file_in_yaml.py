import yaml

dict_file = {"ins_api": {"version": "1.0","type": "cli_show","chunk": "0","sid": "1","input": "show ip interface brief","output_format": "yaml"}}

with open('store_file.yaml', 'w') as file:
    yaml.dump(dict_file, file)