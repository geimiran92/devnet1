### Homework1 ###
import time # Here are the libraries I used for the script #
import os 
import sys
import json
import yaml

# Here I am adding a simple banner #
print(""" 
\n
Welcome!
\n
\nthis tool is used to convert files from \"YAMIL\" to \"JSON\" and viceversa!
\n
""")
time.sleep(2) # This options works with the library "inport os"

def choose_destination_file(): # This is the main funcion
    value_1 = ["yaml", "YAML"] # Here I have the variables used for which file I will need
    value_2 = ["json", "JSON"]
    value_3 = ["quit", "QUIT", "q", "Q"] # A simple log out
    x = input("""Please, enter which Data Type you need:
    \nDo you need a \"YAML\" or \"JSON\ file"?: 
    """ ) # Here I assign a value to direct the script to the file/funcion I chose
    if x in value_1:
        obtain_YAML()
    elif x in value_2:
        obtain_JSON()
    elif x in value_3:
        sys.exit()
    else: # From start if I need a certain file or if I need to close the script
        print("\nNot a valid option. Try again!\n")
        time.sleep(1)
        choose_destination_file()

def obtain_YAML(): # Funcion to get a YAMIL from a Json
    while True: # As long as I enter a correct file option, it will work
        y = input("Enter the source \"JSON\" file name: " ) # Normal and relative path where the device is located
        with open(y) as json_in, open("example.yaml", "w") as yaml_out: # Here I create the needed file and save it in the same folder that the script is located
            json_object = json.load(json_in) # Change to JSON object to start converting
            converted_yaml = yaml.dump(json_object) # From JSON object to YAML object
            yaml_out.write(converted_yaml) # Save the YAML object as file "example.yaml"
            print(converted_yaml) 
            time.sleep(2)
            choose_destination_file() # Run the main funcion to start over

def obtain_JSON(): # Funcion to get a JSON from a YAML
    while True: # As long as I enter a correct file option, it will work
        z = input("Enter the source \"YAML\" file name: " ) # Normal and relative path where the device is located
        with open(z) as yaml_in, open("example.json", "w"): # Here I create the needed file and save it in the same folder that the script is located
            yaml_object = yaml.load(yaml_in) # Change to JSON object to save it and save it into a variable
            print(yaml_object) 
            time.sleep(2)
            choose_destination_file() # Run the main funcion to start over
                
if __name__ == '__main__':
    choose_destination_file() # Here I have the main funcion to start the script
