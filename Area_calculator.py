import math

# equations for the shapes
def calc_rectangle(width, length):
    rect_area = length * width
    return rectangle_area

def calc_trapezoid(top, bottom, height):
    trapezoid_area = ((top + bottom) / 2 ) * height
    return trapezoid_area

def calc_circle(radius):
    circle_area = math.pi * radius ** 2
    return circle_area

def calc_rhombus(height, width):
    rhombus_area = ((height * width) /2)
    return rhombus_area
  
def calc_square(side):
    square_area = side ** 2
    return square_area

def calc_triangle(base, height):
    triangle_area = (base * height) / 2
    return triangle_area

# determining which shape's equation to use
def area_calculator(user_calc):
    if user_calc == "square":
        side = float(input("Enter a number for the length of side: "))
        print (calc_square(side))
    elif user_calc == "circle":
        radius = float(input("Enter a number for the radius: "))
        print (calc_circle(radius))
    elif user_calc == "triangle":
        base = float(input("Enter a number for the length of base: "))
        height = float(input("Enter a number for the height: "))
        print (calc_triangle(base, height))
    elif user_calc == "rectangle":
        length = float(input("Enter a number for the length: "))
        width = float(input("Enter a number for the width: "))
        print (calc_rectangle(width, length))
    elif user_calc == "rhombus":
        height = float(input("Enter a number for the height of the rhombus:"))
        width = float(input("Enter a number for the width of the rhombus:"))
        print (calc_rhombus(height, width))
    elif user_calc == "trapezoid":
        short_base = float(input("Enter a number for the length of the short base: "))
        long_base = float(input("Enter a number for the length of the long base: "))
        height = float(input("Enter a number for the height: "))
        print (calc_trapezoid(top, base, height))
    else:
        area_calculator(input("Error, Re-enter input: "))

# print
print ("This program will calculate the area of shapes for you.")
print ("Shapes available are squares, triangles, rectangle, circles,  and trapezoid.")
print ("Enter square, rectangle, triangle, circle, rhombus or trapezoid.")
area_calculator(input("What area would you like to calculate? "))

