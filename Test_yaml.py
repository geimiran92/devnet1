import yaml

with open('New_file.yaml') as file:
    # The FullLoader parameter handles the conversion from YAML
    # scalar values to Python the dictionary format
    file_contents = yaml.load(file, Loader=yaml.FullLoader)

    print(file_contents)

