### Homework1 ###
import time
import os
import sys
import json
import yaml

print("""
\n
Welcome!
\n
\nthis tool is used to convert files from \"YAMIL\" to \"JSON\" and viceversa!
\n
""")
time.sleep(2)

def choose_destination_file():
    value_1 = ["yaml", "YAML"]
    value_2 = ["json", "JSON"]
    value_3 = ["quit", "QUIT", "q", "Q"]
    x = input("""Please, enter which Data Type you need:
    \nDo you need a \"YAML\" or \"JSON\ file"?: 
    """ )
    if x in value_1:
        obtain_YAML()
    elif x in value_2:
        obtain_JSON()
    elif x in value_3:
        sys.exit()
    else:
        print("\nNot a valid option. Try again!\n")
        time.sleep(1)
        choose_destination_file()

def obtain_YAML():
    while True:
        y = input("Enter the source \"JSON\" file name: " )
        with open(y) as json_in, open("example.yaml", "w") as yaml_out:
            json_object = json.load(json_in)
            converted_yaml = yaml.dump(json_object)
            yaml_out.write(converted_yaml)
            print(converted_yaml)
            time.sleep(2)
            choose_destination_file()

def obtain_JSON():
    while True:
        z = input("Enter the source \"YAML\" file name: " )
        with open(z) as yaml_in, open("example.json", "w"):
            yaml_object = yaml.load(yaml_in)
            print(yaml_object)
            time.sleep(2)
            choose_destination_file()
                
if __name__ == '__main__':
    choose_destination_file()
